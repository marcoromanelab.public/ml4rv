import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ml4rv", # Replace with your own username
    version="1.1.0",
    author="Marco Romanelli",
    author_email="marcoromane.gitlab.public@gmail.com",
    description="Machine Learning for Reverse Vaccinology",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/marcoromane.gitlab.public/ml4rv",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7',
)
