from numpy.random import seed
import pandas as pn
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn import svm
import copy
from sklearn.metrics import classification_report
from utilities import color_print
from sklearn.metrics import roc_curve, roc_auc_score, precision_score, recall_score, confusion_matrix, accuracy_score, \
    balanced_accuracy_score, auc
import matplotlib.pyplot as plt
import numpy as np
import sys
from scipy import interp
import os
import datetime
import json

seed(1234)


#   adapted from https://gist.github.com/zachguo/10296432
def print_cm(cm, labels, log_file, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    """pretty print for confusion matrixes"""
    columnwidth = max([len(x) for x in labels] + [5])  # 5 is value length
    columnwidth = max(columnwidth, len("true\pred"))
    empty_cell = " " * columnwidth
    # Print header
    print "    " + " " * (columnwidth - len("true\pred")) + "true\pred",
    if log_file is not None:
        log_file.write("    " + " " * (columnwidth - len("true\pred")) + "true\pred")
    for label in labels:
        print "%{0}s".format(columnwidth) % label,
        if log_file is not None:
            log_file.write("%{0}s".format(columnwidth) % label)
    print
    if log_file is not None:
        log_file.write("\n")
    # Print rows
    for i, label1 in enumerate(labels):
        print "    %{0}s".format(columnwidth) % label1,
        if log_file is not None:
            log_file.write("    %{0}s".format(columnwidth) % label1)
        for j in range(len(labels)):
            cell = "%{0}.1f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            print cell,
            if log_file is not None:
                log_file.write(cell)
        print
        if log_file is not None:
            log_file.write("\n")


def plot_roc_curve(tprs, aucs, mean_fpr, tprs_single_plots, fprs_single_plots, auc_scores_single_plots, metric_name,
                   show_plot=False, save_plot=False, save_dir=None):
    plt.figure("Metric: " + metric_name)
    for i in range(0, len(tprs_single_plots)):
        plt.plot(fprs_single_plots[i], tprs_single_plots[i], lw=1, alpha=0.3,
                 label='ROC fold %d (AUC = %0.2f)' % (i, auc_scores_single_plots[i]))
    plt.plot([0, 1], [0, 1], color='red', linestyle='--')
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    plt.plot(mean_fpr, mean_tpr, color='b',
             label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)

    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ 1 std. dev.')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC) Curve, metric: ' + metric_name)
    plt.legend()

    if save_plot is True and save_dir is not None:
        plt.savefig(save_dir + str(metric_name) + "_avg_roc.pdf")

    if show_plot is True:
        plt.show()


def create_param_dictionary_rbf_kernel(C_list, gamma_list):
    param_dictionary = ({'kernel': ['rbf'], 'gamma': gamma_list, 'C': C_list})
    return param_dictionary


def print_report_outer_loop(grid_search, test_data, test_supervision, outer_loop_iteration, score, log_file):
    top = "###########################################################################################"
    bottom = top
    msg = "  " + str(score) + ": Outer Loop Iteration #" + str(outer_loop_iteration) + "  "
    len_left = (len(top) - len(msg)) // 2
    if (len(top) - len(msg)) % 2 != 0:
        len_left += 1
    len_right = (len(top) - len(msg)) // 2
    left = bottom[:len_left]
    right = bottom[:len_right]
    complete_msg = left + msg + right
    color_print_man = color_print.ColorPrint()
    color_print_man.color_print(
        string_to_color_print=top,
        color="CYAN")
    color_print_man.color_print(string_to_color_print=complete_msg,
                                color="CYAN")
    color_print_man.color_print(
        string_to_color_print=bottom,
        color="CYAN")

    ########################################################################################################################

    print("# Tuning hyper-parameters for %s" % score)
    print "Best parameters set found on development set:\n"
    print grid_search.best_params_
    print "\n"
    print("Grid scores on development set:\n")
    means = grid_search.cv_results_['mean_test_score']
    stds = grid_search.cv_results_['std_test_score']
    for mean, std, params in zip(means, stds, grid_search.cv_results_['params']):
        print("%0.3f (+/-%0.03f) for %r"
              % (mean, std * 2, params))

    print("\nDetailed classification report:\n")
    print("The model is trained on the full train_val_dataset.")
    print("The scores are computed on the full test_dataset.\n")
    y_true, y_pred = test_supervision, grid_search.predict(test_data)

    print classification_report(y_true=y_true, y_pred=y_pred)

    print "\nConfusion matrix (true_rows, pred_cols, index_growing_order) --->\n", confusion_matrix(y_true=y_true,
                                                                                                    y_pred=y_pred)

    labels_true = np.unique(y_true).tolist()
    labels_pred = np.unique(y_pred).tolist()

    final_labels = list(sorted(set(labels_true + labels_pred)))

    final_labels_str = []
    for el in final_labels:
        final_labels_str.append(str(el))
    cm = confusion_matrix(y_true=y_true, y_pred=y_pred, labels=final_labels)

    if log_file is not None:
        log_file.write("\n" + top + "\n" + complete_msg + "\n" + bottom + "\n")
        log_file.write("# Tuning hyper-parameters for " + str(score) + "\n")
        log_file.write("Best parameters set found on development set:\n")
        log_file.write(json.dumps(grid_search.best_params_))
        log_file.write("\n")
        log_file.write("Grid scores on development set:\n")

        for mean, std, params in zip(means, stds, grid_search.cv_results_['params']):
            to_log_file = str(round(mean, 3)) + " " + str(round(std * 2, 3)) + " " + json.dumps(params) + "\n"
            log_file.write(to_log_file)

        log_file.write("\nDetailed classification report:\n")
        log_file.write("The model is trained on the full train_val_dataset. ")
        log_file.write("The scores are computed on the full test_dataset.\n")

        log_file.write(classification_report(y_true=y_true, y_pred=y_pred))

        log_file.write("\nConfusion matrix (true_rows, pred_cols, index_growing_order) --->\n")
        log_file.write(np.array2string(confusion_matrix(y_true=y_true, y_pred=y_pred)))

        log_file.write("\nConfusion matrix nicer print --->\n")

    print "\nConfusion matrix nicer print --->\n"
    print_cm(cm, labels=final_labels_str, log_file=log_file)

    print "\nPrecision index_growing_order --->\n", precision_score(y_true=y_true, y_pred=y_pred, average=None)
    precision_score_array = precision_score(y_true=y_true, y_pred=y_pred, labels=final_labels, average=None)
    precision_score_dict = {}
    for i in range(0, len(final_labels)):
        precision_score_dict[final_labels[i]] = precision_score_array[i]
    print "\nPrecision nicer print --->\n", precision_score_dict

    print "\nRecall index_growing_order --->\n", recall_score(y_true=y_true, y_pred=y_pred, average=None)
    recall_score_array = recall_score(y_true=y_true, y_pred=y_pred, labels=final_labels, average=None)
    recall_score_dict = {}
    for i in range(0, len(final_labels)):
        recall_score_dict[final_labels[i]] = recall_score_array[i]
    print "\nRecall nicer print --->\n", recall_score_dict

    print "\nAccuracy score ---> ", accuracy_score(y_true, y_pred, normalize=True)
    print "\nBalanced Accuracy score ---> ", balanced_accuracy_score(y_true, y_pred)

    if log_file is not None:
        log_file.write(
            "\nPrecision index_growing_order --->\n" + str(
                precision_score(y_true=y_true, y_pred=y_pred, average=None)) + "\n")
        log_file.write("\nPrecision nicer print --->\n" + json.dumps(precision_score_dict) + "\n")

        log_file.write(
            "\nRecall index_growing_order --->\n" + str(recall_score(y_true=y_true, y_pred=y_pred, average=None)) + "\n")
        log_file.write("\nRecall nicer print --->\n" + json.dumps(recall_score_dict) + "\n")

        log_file.write("\nAccuracy score ---> " + str(accuracy_score(y_true, y_pred, normalize=True)) + "\n")
        log_file.write("\nBalanced Accuracy score ---> " + str(balanced_accuracy_score(y_true, y_pred)) + "\n")


class MLManager:
    def __init__(self, original_dataset, original_supervision, param_dictionary, list_of_scores, k_fold_outer=10,
                 k_fold_inner=10, test_size=.25, val_size=.25, elim_cols=[], n_jobs=-1):
        self.original_dataset = original_dataset
        self.original_supervision = original_supervision

        self.elim_cols = elim_cols
        self.k_fold_outer = k_fold_outer
        self.k_fold_inner = k_fold_inner

        self.test_size = test_size
        self.validation_size = val_size

        self.param_dictionary = param_dictionary

        self.list_of_scores = list_of_scores

        self.n_classes = 0

        self.n_jobs = n_jobs

    def nested_k_fold_CV_hp_optimiz_and_training(self, show_plot=False, save_plot=False, save_dir=None, log_dir=None):
        log_file = None

        if log_dir is not None:
            if log_dir[-1] != '/':
                log_dir += '/'
            if os.path.isdir(log_dir) is False:
                os.system("mkdir " + log_dir)
            date_time = datetime.datetime.now()
            log_file = open(log_dir + "log_" +
                            str(date_time.strftime('%m')) +
                            str(date_time.strftime('%d')) +
                            str(date_time.strftime('%Y')) +
                            "_" +
                            str(date_time.strftime('%H')) +
                            str(date_time.strftime('%M')) +
                            str(date_time.strftime('%S')) +
                            ".txt", "wa")
            log_file.write("ml4rv log: beginning date " + str(date_time) + "\n\n\n")

        if save_plot is True:
            if save_dir is None:
                sys.exit("No input directory for storing the results.")
            if save_dir is not None:
                if save_dir[-1] != '/':
                    save_dir += '/'
                if os.path.isdir(save_dir) is False:
                    os.system("mkdir " + save_dir)

        dataset_for_experiments = copy.deepcopy(self.original_dataset)
        supervision_for_experiments = copy.deepcopy(self.original_supervision)

        if isinstance(dataset_for_experiments, pn.DataFrame):
            dataset_for_experiments = dataset_for_experiments.values

        keep_cols = []
        if len(self.elim_cols) > 0:
            for i in range(0, self.original_dataset.shape[1]):
                if i not in self.elim_cols:
                    keep_cols.append(i)

            dataset_for_experiments = dataset_for_experiments[:, keep_cols]

        if isinstance(supervision_for_experiments, pn.DataFrame):
            supervision_for_experiments = supervision_for_experiments.values

        #   remove warning 1 column vector
        if supervision_for_experiments.shape[1] == 1:
            supervision_for_experiments = supervision_for_experiments.ravel()
            self.n_classes = np.unique(supervision_for_experiments)
        elif supervision_for_experiments.shape[1] > 1:
            self.n_classes = supervision_for_experiments.shape[1]
            tmp = []
            #   if the supervision is encoded use argmax to have a column with all classes
            for i in range(0, supervision_for_experiments.shape[1]):
                tmp.append(np.argmax[supervision_for_experiments[i]])
            supervision_for_experiments = tmp
        else:
            sys.exit("The supervision dimension do not look right, please check if the source file respects the desing.")

        for score in self.list_of_scores:
            tprs = []
            aucs = []
            mean_fpr = np.linspace(0, 1, 100)
            tprs_single_plots = []
            fprs_single_plots = []
            auc_scores_single_plots = []
            #   outer loop
            for outer_fold_iter in range(0, self.k_fold_outer):
                train_val_data, test_data, train_val_supervision, test_supervision = \
                    train_test_split(dataset_for_experiments,
                                     supervision_for_experiments,
                                     test_size=self.test_size,
                                     train_size=None,
                                     random_state=None,
                                     shuffle=True,
                                     stratify=supervision_for_experiments)

                #   inner loop
                param_grid = self.param_dictionary
                grid_search = GridSearchCV(estimator=svm.SVC(),
                                           param_grid=param_grid,
                                           cv=self.k_fold_inner,
                                           scoring=score,
                                           n_jobs=self.n_jobs,
                                           )

                grid_search.fit(train_val_data, train_val_supervision)
                #   end inner loop

                print_report_outer_loop(grid_search=grid_search, test_data=test_data, test_supervision=test_supervision,
                                        outer_loop_iteration=outer_fold_iter, score=score, log_file=log_file)
                best_model_for_this_iteration = svm.SVC(kernel=grid_search.best_params_['kernel'],
                                                        gamma=grid_search.best_params_['gamma'],
                                                        C=grid_search.best_params_['C'], probability=True)
                y_score = best_model_for_this_iteration.fit(train_val_data, train_val_supervision).decision_function(
                    test_data)
                """y_score = best_model_for_this_iteration.fit(train_val_data, train_val_supervision).predict_proba(
                    test_data)[:, 1]"""
                fpr, tpr, thresholds = roc_curve(test_supervision, y_score)
                fprs_single_plots.append(fpr)
                tprs_single_plots.append(tpr)
                auc_score = roc_auc_score(test_supervision, y_score)
                auc_scores_single_plots.append(auc_score)
                tprs.append(interp(mean_fpr, fpr, tpr))
                tprs[-1][0] = 0.0

                #   just to check if it is the same: roc_auc_score and auc give the same results
                roc_auc = auc(fpr, tpr)
                aucs.append(roc_auc)
            plot_roc_curve(tprs=tprs, aucs=aucs, mean_fpr=mean_fpr, tprs_single_plots=tprs_single_plots,
                           fprs_single_plots=fprs_single_plots, auc_scores_single_plots=auc_scores_single_plots,
                           metric_name=score, show_plot=show_plot, save_plot=save_plot, save_dir=save_dir)

        if log_file is not None:
            log_file.write("\n\n\nml4rv log: end date " + str(datetime.datetime.now()))
