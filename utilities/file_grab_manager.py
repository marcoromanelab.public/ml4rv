import pandas as pn
import os
import sys
from utilities import color_print
from numpy.random import seed

seed(1234)


class FileGrabManager:
    def __init__(self):
        self.loaded_object = None
        self.input_file_absolute_path = None
        self.data_df = None
        self.color_print_man = color_print.ColorPrint()

    def reset(self):
        self.loaded_object = None
        self.input_file_absolute_path = None
        self.data_df = None

    #   open file into python object, delete original file if rm_file=True
    def open_file_read(self, input_file_absolute_path, rm_file=False):
        try:
            self.loaded_object = open(name=input_file_absolute_path, mode='r')
            self.input_file_absolute_path = input_file_absolute_path
        except IOError:
            self.color_print_man.color_print(string_to_color_print="Error! Cannot open " + input_file_absolute_path,
                                             color="RED")
            sys.exit()
        else:
            if rm_file is True:
                os.remove(input_file_absolute_path)

    #   read object and load content in a pandas dataframe
    def load_content_on_df(self, input_file_absolute_path, separator, header, rm_file=False, show_all_columns=False):
        self.open_file_read(input_file_absolute_path=input_file_absolute_path, rm_file=rm_file)

        if self.loaded_object is None:
            self.color_print_man.color_print(string_to_color_print="Error: empty loaded object.", color="RED")
            sys.exit()

        else:
            try:
                if header is True:
                    self.data_df = pn.read_csv(filepath_or_buffer=self.loaded_object, sep=separator, header=0)
                elif header is False:
                    self.data_df = pn.read_csv(filepath_or_buffer=self.loaded_object, sep=separator, header=None)
                else:
                    self.color_print_man.color_print(string_to_color_print="Error: wrong value for argument header.",
                                                     color="RED")
                    sys.exit()
            except IOError:
                self.color_print_man.color_print(
                    string_to_color_print="Error! Cannot read " + self.input_file_absolute_path,
                    color="RED")
                self.reset()
                sys.exit()
            else:
                if show_all_columns is True and pn.get_option("display.max_columns") < len(self.data_df.columns):
                    #   increase the amount of pandas visible columns
                    pn.set_option('display.max_columns', len(self.data_df.columns))
                res = self.data_df
                self.reset()
                return res
