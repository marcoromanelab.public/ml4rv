from data_preprocessing import data_preprocessing_class
from utilities import color_print
from numpy.random import seed

seed(1234)


def data_preprocessing(dataset, exclude_cols=[], standardize=False):
    data_preprocessing_manager = data_preprocessing_class.DataPreprocessing(dataset=dataset, exclude_cols=exclude_cols)

    if standardize is True:
        data_preprocessing_manager.standardize_features()

    data_preprocessing_manager.normalize_dataset_between_minus_one_and_one()

    data_preprocessing_manager.from_np_to_df()

    preprocessed_data = data_preprocessing_manager.dataset

    color_print_man = color_print.ColorPrint()
    color_print_man.color_print(string_to_color_print="\n\nPreprocessed Dataset head\n", color="BLUE")
    print preprocessed_data.head()

    return preprocessed_data
