from ml import ml_manager


def ml_section(preprocessed_dataset, dataset_supervision, C_list, gamma_list, list_of_scores, k_fold_outer, k_fold_inner,
               test_size, val_size, elim_cols, n_jobs, show_plot, save_plot, save_dir, log_dir):
    param_dictionary = ml_manager.create_param_dictionary_rbf_kernel(C_list=C_list, gamma_list=gamma_list)
    ml_man = ml_manager.MLManager(original_dataset=preprocessed_dataset, original_supervision=dataset_supervision,
                                  param_dictionary=param_dictionary, list_of_scores=list_of_scores,
                                  k_fold_outer=k_fold_outer,
                                  k_fold_inner=k_fold_inner, test_size=test_size, val_size=val_size, elim_cols=elim_cols,
                                  n_jobs=n_jobs)

    ml_man.nested_k_fold_CV_hp_optimiz_and_training(show_plot=show_plot, save_plot=save_plot, save_dir=save_dir,
                                                    log_dir=log_dir)
