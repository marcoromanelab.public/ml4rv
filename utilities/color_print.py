import sys
from numpy.random import seed
seed(1234)

RED = "\033[1;31m"
BLUE = "\033[1;34m"
CYAN = "\033[1;36m"
GREEN = "\033[1;32m"
RESET = "\033[0;0m"


class ColorPrint:

    def __init__(self):
        self.string_to_color_print = []

    def color_print(self, string_to_color_print, color="RED"):
        self.string_to_color_print = string_to_color_print

        if color not in ["RED", "BLUE", "CYAN", "GREEN"]:
            print "Warning! Selected color not available: try RED, BLUE, CYAN, GREEN"
            print string_to_color_print
            sys.stdout.write(RESET)

        elif color == "RED":
            sys.stdout.write(RED)
            print string_to_color_print
            sys.stdout.write(RESET)

        elif color == "GREEN":
            sys.stdout.write(GREEN)
            print string_to_color_print
            sys.stdout.write(RESET)

        elif color == "CYAN":
            sys.stdout.write(CYAN)
            print string_to_color_print
            sys.stdout.write(RESET)

        elif color == "BLUE":
            sys.stdout.write(BLUE)
            print string_to_color_print
            sys.stdout.write(RESET)
