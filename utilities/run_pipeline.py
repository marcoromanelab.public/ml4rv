from utilities import initialize, preprocessing, ml_section
import numpy as np
import sys
import os

np.random.seed(1234)

DATASET_FILE_ABSOLUTE_PATH = os.path.dirname(os.path.dirname(__file__)) + "/files/DATA_400_HEINSON_reproduced.txt"
SUPERVISION_FILE_ABSOLUTE_PATH = os.path.dirname(
    os.path.dirname(__file__)) + "/files/DATA_400_HEINSON_reproduced_supervision.txt"
C_LIST = [1, 10, 100, 1000]
GAMMA_LIST = [1e-3, 1e-4]
SCORES_LIST = ['roc_auc', 'precision', 'recall']
K_FOLD_OUTER = 10
K_FOLD_INNER = 10
TEST_SIZE = .25
VAL_SIZE = .25
ELIM_COLS = [0]
N_JOBS = -1
SHOW_PLOT = False
SAVE_PLOT = False
SAVE_DIR = os.path.dirname(os.path.dirname(__file__)) + "/files/results/"
LOG_DIR = os.path.dirname(os.path.dirname(__file__)) + "/files/results/"


def exception_call(idx, key_val):
    sys.exit(sys.argv[idx + 1].strip() + " is not a valid argument for option " + key_val)


def read_command_line_options():
    thismodule = sys.modules[__name__]

    for idx, key_val in enumerate(sys.argv, 0):
        # print idx, ", ", key_val
        if key_val in ['--njobs'] and len(sys.argv) > idx + 1:
            try:
                thismodule.N_JOBS = int(sys.argv[idx + 1].strip())
            except ValueError as val_err:
                exception_call(idx=idx, key_val=key_val)

        if key_val in ['--input', '-i'] and len(sys.argv) > idx + 1:
            thismodule.DATASET_FILE_ABSOLUTE_PATH = sys.argv[idx + 1].strip()

        if key_val in ['--supervision', '-s'] and len(sys.argv) > idx + 1:
            thismodule.SUPERVISION_FILE_ABSOLUTE_PATH = sys.argv[idx + 1].strip()

        if key_val in ['--kfoldouter', '-kfo'] and len(sys.argv) > idx + 1:
            try:
                thismodule.K_FOLD_OUTER = int(sys.argv[idx + 1].strip())
            except ValueError as val_err:
                exception_call(idx=idx, key_val=key_val)

        if key_val in ['--kfoldinner', '-kfi'] and len(sys.argv) > idx + 1:
            try:
                thismodule.K_FOLD_INNER = int(sys.argv[idx + 1].strip())
            except ValueError as val_err:
                exception_call(idx=idx, key_val=key_val)

        if key_val in ['--testsize', '-ts'] and len(sys.argv) > idx + 1:
            try:
                thismodule.TEST_SIZE = float(sys.argv[idx + 1].strip())
            except ValueError as val_err:
                exception_call(idx=idx, key_val=key_val)

        if key_val in ['--validationsize', '-vs'] and len(sys.argv) > idx + 1:
            try:
                thismodule.VAL_SIZE = float(sys.argv[idx + 1].strip())
            except ValueError as val_err:
                exception_call(idx=idx, key_val=key_val)

        if key_val in ['--clist', '-cl'] and len(sys.argv) > idx + 1:
            #   it must be something like -cl (1,2,3)
            string_to_be_adapted = sys.argv[idx + 1].strip()
            # print string_to_be_adapted
            if string_to_be_adapted[0] != '(' or string_to_be_adapted[-1] != ')':
                exception_call(idx=idx, key_val=key_val)
            else:
                strip_string_to_be_adapted = string_to_be_adapted[1:-1]
                split_list = strip_string_to_be_adapted.split(',')
                if len(split_list) != 3:
                    exception_call(idx=idx, key_val=key_val)
                else:
                    try:
                        start = float(split_list[0])
                        stop = float(split_list[1])
                        step = float(split_list[2])
                        thismodule.C_LIST = np.arange(start, stop, step)
                    except ValueError as val_err:
                        exception_call(idx=idx, key_val=key_val)

        if key_val in ['--gammalist', '-gl'] and len(sys.argv) > idx + 1:
            #   it must be something like -cl (1,2,3)
            string_to_be_adapted = sys.argv[idx + 1].strip()
            # print string_to_be_adapted
            if string_to_be_adapted[0] != '(' or string_to_be_adapted[-1] != ')':
                exception_call(idx=idx, key_val=key_val)
            else:
                strip_string_to_be_adapted = string_to_be_adapted[1:-1]
                split_list = strip_string_to_be_adapted.split(',')
                if len(split_list) != 3:
                    exception_call(idx=idx, key_val=key_val)
                else:
                    try:
                        start = float(split_list[0])
                        stop = float(split_list[1])
                        step = float(split_list[2])
                        thismodule.GAMMA_LIST = np.arange(start, stop, step)
                    except ValueError as val_err:
                        exception_call(idx=idx, key_val=key_val)

        if key_val in ['--delcols', '-dc'] and len(sys.argv) > idx + 1:
            #   it must be something like -dc [1,2,3]
            string_to_be_adapted = sys.argv[idx + 1].strip()
            # print string_to_be_adapted
            if string_to_be_adapted[0] != '[' or string_to_be_adapted[-1] != ']':
                exception_call(idx=idx, key_val=key_val)
            else:
                strip_string_to_be_adapted = string_to_be_adapted[1:-1]
                split_list = strip_string_to_be_adapted.split(',')
                ELIM_COLS_ = []
                for item in split_list:
                    try:
                        ELIM_COLS_.append(int(item))
                    except ValueError as val_err:
                        exception_call(idx=idx, key_val=key_val)
                thismodule.ELIM_COLS = ELIM_COLS_

        if key_val in ['--scoreslist', '-sl'] and len(sys.argv) > idx + 1:
            #   it must be something like -sl [roc_auc,precision,recall]
            string_to_be_adapted = sys.argv[idx + 1].strip()
            # print string_to_be_adapted
            if string_to_be_adapted[0] != '[' or string_to_be_adapted[-1] != ']':
                exception_call(idx=idx, key_val=key_val)
            else:
                strip_string_to_be_adapted = string_to_be_adapted[1:-1]
                split_list = strip_string_to_be_adapted.split(',')
                SCORES_LIST_ = []
                for item in split_list:
                    try:
                        SCORES_LIST_.append(str(item))
                    except ValueError as val_err:
                        exception_call(idx=idx, key_val=key_val)
                thismodule.SCORES_LIST = SCORES_LIST_

        if key_val in ['--showplot', '-sp']:
            thismodule.SHOW_PLOT = True

        if key_val in ['--saveplot', '-svp'] and len(sys.argv) > idx + 1:
            thismodule.SAVE_PLOT = True
            thismodule.SAVE_DIR = sys.argv[idx + 1].strip()

        if key_val in ['--log'] and len(sys.argv) > idx + 1:
            thismodule.LOG_DIR = sys.argv[idx + 1].strip()


def run_pipeline():
    print "\n\n\n\n\n\n"
    print sys.argv, "\n\n\n\n\n\n"

    read_command_line_options()

    dataset, dataset_supervision = initialize.initialize(dataset_file_absolute_path=DATASET_FILE_ABSOLUTE_PATH,
                                                         supervision_file_absolute_path=SUPERVISION_FILE_ABSOLUTE_PATH)

    preprocessed_dataset = preprocessing.data_preprocessing(dataset=dataset, exclude_cols=ELIM_COLS, standardize=False)

    ml_section.ml_section(preprocessed_dataset=preprocessed_dataset, dataset_supervision=dataset_supervision,
                          # C_list=np.arange(2 ** (-5), 2 ** 15, 0.5), gamma_list=np.arange(2 ** (-15), 2 ** 3, 0.5),
                          C_list=C_LIST, gamma_list=GAMMA_LIST,
                          list_of_scores=['roc_auc', 'precision', 'recall'], k_fold_outer=K_FOLD_OUTER,
                          k_fold_inner=K_FOLD_INNER,
                          test_size=TEST_SIZE, val_size=VAL_SIZE, elim_cols=ELIM_COLS, n_jobs=N_JOBS,
                          show_plot=SHOW_PLOT, save_plot=SAVE_PLOT, save_dir=SAVE_DIR, log_dir=LOG_DIR)
