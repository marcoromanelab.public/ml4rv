"""
static functon to handle the initialization of the program
"""

from utilities import file_grab_manager, color_print
from numpy.random import seed


def initialize(dataset_file_absolute_path, supervision_file_absolute_path):
    """
    TODO: ask for files
    """
    seed(1234)

    color_print_man = color_print.ColorPrint()

    color_print_man.color_print(
        string_to_color_print="Welcome, earthling!", color="BLUE")

    file_grab_man = file_grab_manager.FileGrabManager()

    dataset = file_grab_man.load_content_on_df(
        input_file_absolute_path=dataset_file_absolute_path,
        separator="\t", header=True, rm_file=False, show_all_columns=True)

    color_print_man.color_print(string_to_color_print="\n\nDataset head\n", color="BLUE")
    print dataset.head()

    dataset_supervision = file_grab_man.load_content_on_df(
        input_file_absolute_path=supervision_file_absolute_path,
        separator="\t", header=True, rm_file=False, show_all_columns=True)

    color_print_man.color_print(string_to_color_print="\n\nDataset Supervision head\n", color="BLUE")
    print dataset_supervision.head()

    return [dataset, dataset_supervision]
