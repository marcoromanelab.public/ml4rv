# __ML4RV - Machine Learning for Reverse Vaccinology__

<div align="center">
    <img src="figures/ml4rv_icon-1.jpeg" width="300" height=300/>
</div>

## Overview
Machine Learning for Reverse Vaccinology is a Python tool inspired by 
**Enhancing the Biological Relevance of Machine Learning Classifiers for Reverse Vaccinology**, 
Heinson et al., *Journal of Molecular Science*, 2017.


## Starting point
In 2017, Heinson et al. published a reverse vaccinology pipeline based on SVMs with radial bias 
function (RBF) kernel.

The classifier was evaluated using a nested Leave-Ten-Out Cross-Validation (LTOCV) model 
using a dataset of 200 bacterial protective antigens found in literature (+cases) and 200 randomly 
selected proteins (-cases) with less than 98% of sequence similarity.
The initial set of 525 features to describe the proteins was reduced to a 10 proteins set 
via backward elimination.

The top ten feautures selected by greedy backward feature elimination for discrimination of +cases 
and -cases are:

1. LipoP_Signal_Avr_Length (LipoP)
2. YinOYang-T-Count (YinOYang)
3. NetPhosK-S-Count (NetPhosK)
4. LipoP_SPI_Avr_Length (LipoP)
5. NetMhcPan-B-AvgRank (NetMhc)
6. TargetP-SecretFlag (TargetP)
7. YinOYang-Average-Difference1_Length (YinOYang)
8. MBAAgl7_CorCount (GPS-MBA)
9. PickPocket-Average_score (PickPocket)
10. PropFurin-Count_Score (ProP)

*(in brackets the programs used to extract the feature)*

Utilizing these 10 features they reached a AUC of 78% using the 
nested Leave-Ten-Out Cross-Validation (nLTOCV).

## Software description
The provided software can be executed both as jupyter notebook and from command line. 
We used Python 2.7.15 and the following packages: numpy, pandas, scikit-learn, matplotlib, pickle.
We suggest that the users keep the packages up to date with the most recently released version. 

While the jupyter notebook version is more user friendly and easy to use, the command line version 
comes with many command line options which make the tool more flexible at running time. We are going to 
list all the option in the next subparagraph.
### Command line options
The script can be run launching the line command:
```console
foo@bar:~$ python main.py
```
In order to speed up the execution run:
```console
foo@bar:~$ python main.py --njobs 4
```
This command sets the number of parallel jobs to be executed, check 
[this](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html) out 
for further details. 
To pass the input file by path execute:
```console
foo@bar:~$ python main.py --input <absolute_path>
```
or 
```console
foo@bar:~$ python main.py -i <absolute_path>
```
To pass the supervision file by path execute:
```console
foo@bar:~$ python main.py --supervision <absolute_path>
```
or 
```console
foo@bar:~$ python main.py -s <absolute_path>
```
To set the outer folders for the nLTOCV, run
```console
foo@bar:~$ python main.py --kfoldouter <integer_number>
```
or
```console
foo@bar:~$ python main.py --kfo <integer_number>
```
To set the inner folders for the nLTOCV, run
```console
foo@bar:~$ python main.py --kfoldinner <integer_number>
```
or
```console
foo@bar:~$ python main.py --kfi <integer_number>
```
To set the size for the validation and test data run (stratification):
```console
foo@bar:~$ python main.py --testsize <float_number_between_0_and_1> --validationsize <float_number_between_0_and_1>
```
or the short version:
```console
foo@bar:~$ python main.py -ts <float_number_between_0_and_1> -vs <float_number_between_0_and_1>
```
To pass the list of values for the cost parameter of the SVM, run:
```console
foo@bar:~$ python main.py --clist <(C_0, C_1, ..., C_n)>
```
or 
```console
foo@bar:~$ python main.py -cl <(C_0, C_1, ..., C_n)>
```
The same goes for the list of gamma parameters, but the option is 
*--gammalist* or *-gl*
In order to delete the columns of the dataset which are not of
interest for us it is possible to run 
```console
foo@bar:~$ python main.py --delcols <[col_0, ..., col_m]>
```
or the short version *-dc*.
The command to set the list of scores for the performance evaluation
is, for example, the following:
```console
foo@bar:~$ python main.py --scoreslist <[roc_auc,precision,recall]>
```
or 
```console
foo@bar:~$ python main.py -sl <[roc_auc,precision,recall]>
```
In order to show the plots run the program with the following option
```console
foo@bar:~$ python main.py --showplot <[roc_auc,precision,recall]>
```
The short version for this option is *-sp*.
To save the plots and the log file run the program as:
```console
foo@bar:~$ python main.py --log <absolute_path_to_the_folder_where_the_log_must_be_stored> --saveplot <absolute_path_to_the_folder_where_the_plots_must_be_stored>
```
or use the short version *-svp* to save the plots.
