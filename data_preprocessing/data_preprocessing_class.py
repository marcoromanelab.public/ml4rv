from sklearn import preprocessing
import copy
import pandas as pn
from numpy.random import seed

seed(1234)


class DataPreprocessing:
    def __init__(self, dataset, exclude_cols=[]):
        #   create a copy of the original dataset to avoid unwanted modifications, represent is as array of arrays
        self.dataset = copy.deepcopy(x=dataset)
        if isinstance(self.dataset, pn.DataFrame):
            self.dataset = self.dataset.values
        self.col_names = dataset.columns.values
        self.exclude_cols = exclude_cols

    def select_cols(self):
        #   create list of columns to be scaled
        selected_columns = []
        for column_idx in range(0, self.dataset.shape[1]):
            if column_idx not in self.exclude_cols:
                selected_columns.append(column_idx)

        #   only consider the selected columns for the scaling procedure
        tmp = self.dataset[:, selected_columns]
        tmp = tmp.astype('float64')

        return tmp

    def recompose_dataset(self, tmp):
        #   put together both the scaled and unscaled columns
        selected_columns_counter = 0
        for column_idx in range(0, self.dataset.shape[1]):
            if column_idx not in self.exclude_cols:
                self.dataset[:, column_idx] = tmp[:, selected_columns_counter]
                selected_columns_counter += 1
            else:
                pass

        return None

    #   scale according to x=(x-mu)/sigma
    def standardize_features(self):
        tmp = self.select_cols()
        tmp_standardized = preprocessing.scale(tmp)

        self.recompose_dataset(tmp=tmp_standardized)

        return None

    #   normalize according to (2*(x-min)/(max-min))-1
    def normalize_dataset_between_minus_one_and_one(self):
        #   prepare the scaler manager
        min_max_scaler = preprocessing.MinMaxScaler(feature_range=(-1, 1))

        tmp = self.select_cols()
        tmp_scaled = min_max_scaler.fit_transform(X=tmp)

        self.recompose_dataset(tmp=tmp_scaled)

        return None

    def from_np_to_df(self):
        self.dataset = pn.DataFrame(data=self.dataset, columns=self.col_names)
